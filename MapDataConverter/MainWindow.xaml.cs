﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using MapDataConverter.XmlData;
using Microsoft.Win32;
using SWD_Transport.Data;

namespace MapDataConverter
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public ObservableCollection<linie> Linies { get; }

        public MainWindow()
        {
            Linies = new ObservableCollection<linie>();
            InitializeComponent();
            List.ItemsSource = Linies;
        }

        public List<linie> ReadFiles()
        {
            List<linie> linies = new List<linie>();
            OpenFileDialog openFileDialog = new OpenFileDialog
            {
                Filter = "Plik XML (*.xml)|*.xml",
                CheckPathExists = true,
                Multiselect = true
            };
            if (openFileDialog.ShowDialog() == true)
            {
                var collection = openFileDialog.OpenFiles().Select(Utils.ReadLines).Where(l => l != null).ToList();
                linies.AddRange(collection);
                if (collection.Count > 0)
                {
                    string s = collection.Aggregate("", (current, linie) => current + linie.linia.nazwa + ", ");
                    s = s.Substring(0, s.Length - 2);
                    MessageBox.Show("Wczytano:\n" + s, "Zakończono wczytywanie linii");
                }
                else
                {
                    MessageBox.Show("Połączenia nie zostały wczytane", "Błąd odczytu pliku (plików)");
                }
            }
            return linies;
        }

        private void ReadButton_OnClick(object sender, RoutedEventArgs e)
        {
            ReadFiles().Where(linie => linie != null).ToList().ForEach(Linies.Add);
        }

        private void SaveButton_OnClick(object sender, RoutedEventArgs e)
        {
            MapData mapData = new MapData();
            foreach (linie linie in Linies.Where(linie => linie != null))
            {
                Utils.Convert(linie.linia, mapData);
            }
            mapData.ResolveDepenencies();
            SaveFileDialog saveFileDialog = new SaveFileDialog
            {
                Filter = "Plik XML (*.xml)|*.xml",
                CheckPathExists = true
            };
            if (saveFileDialog.ShowDialog() == true)
            {
                var saved = SWD_Transport.Logic.Utils.SaveData(mapData, saveFileDialog.OpenFile());
                if (saved)
                {
                    MessageBox.Show("Wybrane linie zostały zapisane", "Zapisano");
                }
                else
                {
                    MessageBox.Show("Połączenia nie zostały zapisane", "Błąd zapisu pliku");
                }
            }
        }

        private void ClearButton_OnClick(object sender, RoutedEventArgs e)
        {
            Linies.Clear();
        }
    }
}