﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using MapDataConverter.XmlData;
using SWD_Transport.Data;

namespace MapDataConverter
{
    public static class Utils
    {
        public static MapData Convert(linieLinia linia)
        {
            return Convert(linia, new MapData());
        }

        public static MapData Convert(linieLinia linia, MapData mapData)
        {
            TransportLine transportLine = new TransportLine(linia.nazwa);
            if (!mapData.Lines.Contains(transportLine))
                mapData.Lines.Add(transportLine);
            //Console.WriteLine(linia.nazwa);
            for (var i = 0; i < linia.wariant.Length && i < 2; i++)
            {
                var wariant = linia.wariant[i];
                for (var j = 0; j < wariant.przystanek.Length - 1; j++)
                {
                    var przystanek = wariant.przystanek[j];
                    var next = wariant.przystanek[j + 1];
                    var start = new Stop(przystanek.nazwa);
                    var end = new Stop(next.nazwa);
                    if (przystanek?.czasy?.Length > 1)
                    {
                        foreach (var dzien in przystanek.tabliczka.dzien)
                        {
                            List<DayOfWeek> daysOfWeek = Days(dzien.nazwa);
                            if (daysOfWeek.Count == 0) continue;
                            foreach (var godz in dzien.godz)
                            {
                                foreach (var min in godz.min.Where(m => m.ozn == null))
                                {
                                    var startTime = new Time(godz.h, min.m);
                                    Time endTime;

                                    if (next.tabliczka != null)
                                    {
                                        var nextHour = next.tabliczka.dzien.First(d => d.nazwa.Equals(dzien.nazwa))
                                            ?.godz
                                            ?.First(g => g.h >= godz.h &&
                                                         g.min.Where(m => m.ozn == null)
                                                             .Any(m => m.m + (g.h - godz.h) * 60 >= min.m));
                                        if (nextHour == null)
                                            continue;
                                        var nextMin =
                                            nextHour.min.Where(m => m.ozn == null)
                                                .First(m => m.m + (nextHour.h - godz.h) * 60 >= min.m);

                                        endTime = new Time(nextHour.h, nextMin.m);
                                    }
                                    else
                                    {
                                        endTime = new Time(godz.h, min.m + przystanek.czasy[1].czas);
                                    }

                                    endTime.FixFormat();
                                    foreach (DayOfWeek dayOfWeek in daysOfWeek)
                                    {
                                        mapData.Connections.Add(new Connection(start, end, startTime, endTime,
                                            transportLine, dayOfWeek));
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return mapData;
        }

        public static List<DayOfWeek> Days(string day)
        {
            List<DayOfWeek> days = new List<DayOfWeek>();

            if (day.Equals("w dni robocze"))
            {
                days.Add(DayOfWeek.Monday);
                days.Add(DayOfWeek.Tuesday);
                days.Add(DayOfWeek.Wednesday);
                days.Add(DayOfWeek.Thursday);
                days.Add(DayOfWeek.Friday);
            }
            else if (day.Equals("Sobota"))
            {
                days.Add(DayOfWeek.Saturday);
            }
            else if (day.Equals("Niedziela"))
            {
                days.Add(DayOfWeek.Sunday);
            }
            return days;
        }

        public static linie ReadLines(Stream path)
        {
            try
            {
                StreamReader streamReader = new StreamReader(path);
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(linie));
                linie d = xmlSerializer.Deserialize(streamReader.BaseStream) as linie;
                return d;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static linie ReadLines(string path)
        {
            try
            {
                StreamReader streamReader = new StreamReader(path);
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(linie));
                linie d = xmlSerializer.Deserialize(streamReader.BaseStream) as linie;
                return d;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}