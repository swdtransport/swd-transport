using System;

namespace MapDataConverter.XmlData
{
    /// <remarks/>
    [Serializable()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class linie
    {
        private linieLinia liniaField;

        /// <remarks/>
        public linieLinia linia
        {
            get { return this.liniaField; }
            set { this.liniaField = value; }
        }

        public override string ToString()
        {
            return linia.nazwa;
        }
    }
}