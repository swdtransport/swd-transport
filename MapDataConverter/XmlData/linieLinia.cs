using System;

namespace MapDataConverter.XmlData
{
    /// <remarks/>
    [Serializable()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class linieLinia
    {
        private linieLiniaWariant[] wariantField;

        private string nazwaField;

        private string typField;

        private string wazny_odField;

        private string wazny_doField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("wariant")]
        public linieLiniaWariant[] wariant
        {
            get { return this.wariantField; }
            set { this.wariantField = value; }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string nazwa
        {
            get { return this.nazwaField; }
            set { this.nazwaField = value; }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string typ
        {
            get { return this.typField; }
            set { this.typField = value; }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string wazny_od
        {
            get { return this.wazny_odField; }
            set { this.wazny_odField = value; }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string wazny_do
        {
            get { return this.wazny_doField; }
            set { this.wazny_doField = value; }
        }
    }
}