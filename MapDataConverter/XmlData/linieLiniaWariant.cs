using System;

namespace MapDataConverter.XmlData
{
    /// <remarks/>
    [Serializable()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class linieLiniaWariant
    {

        private linieLiniaWariantPrzystanek[] przystanekField;

        private int idField;

        private string nazwaField;

        private string objField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("przystanek")]
        public linieLiniaWariantPrzystanek[] przystanek
        {
            get
            {
                return this.przystanekField;
            }
            set
            {
                this.przystanekField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string nazwa
        {
            get
            {
                return this.nazwaField;
            }
            set
            {
                this.nazwaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string obj
        {
            get
            {
                return this.objField;
            }
            set
            {
                this.objField = value;
            }
        }
    }
}