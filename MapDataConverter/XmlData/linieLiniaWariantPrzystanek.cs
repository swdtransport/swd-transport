using System;

namespace MapDataConverter.XmlData
{
    /// <remarks/>
    [Serializable()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class linieLiniaWariantPrzystanek
    {

        private linieLiniaWariantPrzystanekPrzystanek[] czasyField;

        private linieLiniaWariantPrzystanekTabliczka tabliczkaField;

        private int idField;

        private string nazwaField;

        private string ulicaField;

        private string cechyField;

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("przystanek", IsNullable = false)]
        public linieLiniaWariantPrzystanekPrzystanek[] czasy
        {
            get
            {
                return this.czasyField;
            }
            set
            {
                this.czasyField = value;
            }
        }

        /// <remarks/>
        public linieLiniaWariantPrzystanekTabliczka tabliczka
        {
            get
            {
                return this.tabliczkaField;
            }
            set
            {
                this.tabliczkaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string nazwa
        {
            get
            {
                return this.nazwaField;
            }
            set
            {
                this.nazwaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ulica
        {
            get
            {
                return this.ulicaField;
            }
            set
            {
                this.ulicaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string cechy
        {
            get
            {
                return this.cechyField;
            }
            set
            {
                this.cechyField = value;
            }
        }
    }
}