using System;

namespace MapDataConverter.XmlData
{
    /// <remarks/>
    [Serializable()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class linieLiniaWariantPrzystanekPrzystanek
    {

        private int numerField;

        private int idField;

        private string nazwaField;

        private int czasField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int numer
        {
            get
            {
                return this.numerField;
            }
            set
            {
                this.numerField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string nazwa
        {
            get
            {
                return this.nazwaField;
            }
            set
            {
                this.nazwaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int czas
        {
            get
            {
                return this.czasField;
            }
            set
            {
                this.czasField = value;
            }
        }
    }
}