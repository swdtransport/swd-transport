using System;

namespace MapDataConverter.XmlData
{
    /// <remarks/>
    [Serializable()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class linieLiniaWariantPrzystanekTabliczka
    {

        private linieLiniaWariantPrzystanekTabliczkaDzien[] dzienField;

        private int idField;

        private int mcField;

        private string cechyField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("dzien")]
        public linieLiniaWariantPrzystanekTabliczkaDzien[] dzien
        {
            get
            {
                return this.dzienField;
            }
            set
            {
                this.dzienField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int mc
        {
            get
            {
                return this.mcField;
            }
            set
            {
                this.mcField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string cechy
        {
            get
            {
                return this.cechyField;
            }
            set
            {
                this.cechyField = value;
            }
        }
    }
}