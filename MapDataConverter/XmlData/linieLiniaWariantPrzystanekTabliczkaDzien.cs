using System;

namespace MapDataConverter.XmlData
{
    /// <remarks/>
    [Serializable()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class linieLiniaWariantPrzystanekTabliczkaDzien
    {

        private linieLiniaWariantPrzystanekTabliczkaDzienGodz[] godzField;

        private string nazwaField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("godz")]
        public linieLiniaWariantPrzystanekTabliczkaDzienGodz[] godz
        {
            get
            {
                return this.godzField;
            }
            set
            {
                this.godzField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string nazwa
        {
            get
            {
                return this.nazwaField;
            }
            set
            {
                this.nazwaField = value;
            }
        }
    }
}