using System;

namespace MapDataConverter.XmlData
{
    /// <remarks/>
    [Serializable()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class linieLiniaWariantPrzystanekTabliczkaDzienGodz
    {

        private linieLiniaWariantPrzystanekTabliczkaDzienGodzMin[] minField;

        private int hField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("min")]
        public linieLiniaWariantPrzystanekTabliczkaDzienGodzMin[] min
        {
            get
            {
                return this.minField;
            }
            set
            {
                this.minField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int h
        {
            get
            {
                return this.hField;
            }
            set
            {
                this.hField = value;
            }
        }
    }
}