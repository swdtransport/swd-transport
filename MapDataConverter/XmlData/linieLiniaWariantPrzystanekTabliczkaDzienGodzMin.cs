using System;

namespace MapDataConverter.XmlData
{
    /// <remarks/>
    [Serializable()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class linieLiniaWariantPrzystanekTabliczkaDzienGodzMin
    {

        private int mField;

        private string oznField;

        private string przypField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int m
        {
            get
            {
                return this.mField;
            }
            set
            {
                this.mField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ozn
        {
            get
            {
                return this.oznField;
            }
            set
            {
                this.oznField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string przyp
        {
            get
            {
                return this.przypField;
            }
            set
            {
                this.przypField = value;
            }
        }
    }
}