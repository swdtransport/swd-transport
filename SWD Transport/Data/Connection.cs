﻿using System;

namespace SWD_Transport.Data
{
    public class Connection
    {
        public Stop Start { get; set; }
        public Stop End { get; set; }
        public Time StartTime { get; set; }
        public Time EndTime { get; set; }
        public TransportLine Line { get; set; }
        public DayOfWeek DayOfWeek { get; set; }

        public Connection()
        {
        }

        public Connection(Stop start, Stop end, Time startTime, Time endTime, TransportLine line, DayOfWeek dayOfWeek)
        {
            Start = start;
            End = end;
            StartTime = startTime;
            EndTime = endTime;
            Line = line;
            DayOfWeek = dayOfWeek;
        }

        public override string ToString()
        {
            return
                $"{nameof(Start)}: {Start}, {nameof(End)}: {End}, {nameof(StartTime)}: {StartTime}, {nameof(EndTime)}: {EndTime}, {nameof(Line)}: {Line}, {nameof(DayOfWeek)}: {DayOfWeek}";
        }

        protected bool Equals(Connection other)
        {
            return Equals(Start, other.Start) && Equals(End, other.End) && Equals(StartTime, other.StartTime) && Equals(EndTime, other.EndTime) && Equals(Line, other.Line) && DayOfWeek == other.DayOfWeek;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Connection) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (Start != null ? Start.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (End != null ? End.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (StartTime != null ? StartTime.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (EndTime != null ? EndTime.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Line != null ? Line.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (int) DayOfWeek;
                return hashCode;
            }
        }
    }
}