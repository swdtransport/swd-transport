﻿using System;
using System.Collections.Generic;

namespace SWD_Transport.Data
{
    public class ConnectionsGroup
    {
        public List<Connection> Connections { get; set; }
        public TransportLine Line { get; set; }
        public Time StartTime { get; set; }
        public Time EndTime { get; set; }
        public Stop Start { get; set; }
        public Stop End { get; set; }



        public ConnectionsGroup(List<Connection> connections)
        {
            var first = connections[0];
            var last = connections[connections.Count - 1];
            if (connections.Count==0 || !connections.TrueForAll(c => c.Line.Equals(first.Line)))
                throw new ArgumentException("Bad Group");
            Connections = connections;
            Line = first.Line;
            StartTime = first.StartTime;
            EndTime = last.EndTime;
            Start = first.Start;
            End = last.End;
        }
    }
}
