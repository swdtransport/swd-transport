﻿namespace SWD_Transport.Data
{
    public class Decision
    {
        public Stop Stop { get; set; }
        public TransportLine Line { get; set; }

        public Decision(Stop st, TransportLine tl)
        {
            Stop = st;
            Line = tl;
        }

        public override string ToString()
        {
            return "( " + Stop.Name +  ", " + Line.Name + " )";
        }
    }
}
