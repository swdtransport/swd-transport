﻿using System;
using System.Collections.Generic;
using System.Linq;
using SWD_Transport.Logic;

namespace SWD_Transport.Data
{
    public class MapData
    {
        public List<TransportLine> Lines { get; set; }
        public List<Stop> Stops { get; set; }
        public List<Connection> Connections { get; set; }

        public MapData()
        {
            Lines = new List<TransportLine>();
            Stops = new List<Stop>();
            Connections = new List<Connection>();
        }

        public MapData(List<TransportLine> lines, List<Stop> stops, List<Connection> connections)
        {
            Lines = lines;
            Stops = stops;
            Connections = connections;
        }

        public void ResolveDepenencies()
        {
            Lines.AddRange(Connections.Where(connection => !Lines.Contains(connection.Line)).Select(c => c.Line)
                .AsEnumerable());
            Stops.AddRange(Connections.Where(connection => !Stops.Contains(connection.Start)).Select(c => c.Start)
                .AsEnumerable());
            Stops.AddRange(Connections.Where(connection => !Stops.Contains(connection.End)).Select(c => c.End)
                .AsEnumerable());
            Connections = Connections.Distinct().ToList();
            Lines = Lines.Distinct().ToList();
            Stops = Stops.Distinct().ToList();
        }

        public void AddWalks()
        {
            var walkConnection = Connections.Where(c => c.Line.Equals(Utils.Walking)).ToList();
            foreach (var walk in walkConnection)
            {
                int length = walk.EndTime.MinutesFromMidnight - walk.StartTime.MinutesFromMidnight;
                for (int i = 0; i < 1440 - length; i++)
                {
                    Time start = new Time(0, i);
                    Time end = new Time(0, i + length);
                    start.FixFormat();
                    end.FixFormat();
                    //                    Console.WriteLine(start + "  -  " + end);
                    foreach (var dayOfWeek in Enum.GetValues(typeof(DayOfWeek)))
                    {
                        //                        Console.Write(dayOfWeek + ", ");
                        Connections.Add(new Connection(walk.Start, walk.End, start, end, Utils.Walking,
                            (DayOfWeek) dayOfWeek));
                    }
                    //                    Console.WriteLine();
                }
            }
            ResolveDepenencies();
        }

        public void RemoveWalks()
        {
            Connections.RemoveAll(c => c.Line.Equals(Utils.Walking) &&
                                       (!c.DayOfWeek.Equals(DayOfWeek.Monday) || c.StartTime.MinutesFromMidnight != 0));
        }
    }
}