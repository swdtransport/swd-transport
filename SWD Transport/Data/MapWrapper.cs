﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SWD_Transport.Data
{
    public class MapWrapper
    {
        private readonly Dictionary<Stop, Dictionary<DayOfWeek, List<Connection>>> _data;
        private static TransportLine _nullLine = new TransportLine("start");

        public MapWrapper(MapData map)
        {
            _data = new Dictionary<Stop, Dictionary<DayOfWeek, List<Connection>>>();
            foreach (Connection connection in map.Connections)
            {
                if (!_data.ContainsKey(connection.End))
                {
                    _data.Add(connection.End, new Dictionary<DayOfWeek, List<Connection>>());
                }
                if (!_data[connection.End].ContainsKey(connection.DayOfWeek))
                {
                    _data[connection.End].Add(connection.DayOfWeek, new List<Connection>());
                }
                _data[connection.End][connection.DayOfWeek].Add(connection);
            }
            foreach (KeyValuePair<DayOfWeek, List<Connection>> dayValue in _data.SelectMany(
                stopValue => stopValue.Value))
            {
                dayValue.Value.Sort((c1, c2) => -c1.EndTime.CompareTo(c2.EndTime));
            }
            //Console.WriteLine("Created Map Wrapper");
        }

        public List<Connection> GetConnectionsForStop(Stop destination)
        {
            if (!_data.ContainsKey(destination))
                return new List<Connection>();
            return _data[destination].Values.ToList().SelectMany(c => c).ToList();
        }

        public List<Connection> GetConnectionsForStopAndDay(Stop destination, DayOfWeek dayOfWeek)
        {
            if (!_data.ContainsKey(destination) || !_data[destination].ContainsKey(dayOfWeek))
                return new List<Connection>();
            return _data[destination][dayOfWeek];
        }

        public List<Connection> GetConnectionsFiltered(Stop destination, DayOfWeek dayOfWeek, Time endTime,
            Time startTime)
        {
            if (!_data.ContainsKey(destination) || !_data[destination].ContainsKey(dayOfWeek))
                return new List<Connection>();
            return _data[destination][dayOfWeek].Where(c => c.EndTime <= endTime && c.StartTime >= startTime).ToList();
        }

        public List<Connection> GetConnectionsBetween(Stop start, List<Stop> destinations, DayOfWeek dayOfWeek,
            Time endTime, Time startTime)
        {
            List<Connection> connections = new List<Connection>();
            foreach (Stop stop in destinations)
            {
                connections.AddRange(GetConnectionsFiltered(stop, dayOfWeek, endTime, startTime)
                    .Where(c => c.Start.Equals(start) && !c.Line.Equals(_nullLine)));
            }
            return connections;
        }

        public List<Connection> GetPreviousConnections(Stop destination, DayOfWeek dayOfWeek, Time endTime,
            Time startTime)
        {
            return GetConnectionsFiltered(destination, dayOfWeek, endTime, startTime)
                .SelectMany(c => GetConnectionsFiltered(c.Start, dayOfWeek, endTime, startTime)).Distinct().ToList();
            //return DisctinctLine(GetConnectionsFiltered(destination, dayOfWeek, endTime, startTime)
            //    .SelectMany(c => GetConnectionsFiltered(c.Start, dayOfWeek, endTime, startTime)).Distinct().ToList());
        }

        private static List<Connection> DisctinctLine(List<Connection> connections)
        {
            List<Connection> toReturn = new List<Connection>();
            foreach (Connection connection in connections)
            {
                if (toReturn.Any(c => c.Start.Equals(connection.Start) && c.End.Equals(connection.End) &&
                                      c.Line.Equals(connection.Line)))
                    continue;
                toReturn.Add(connection);
            }
            return toReturn;
        }
    }
}