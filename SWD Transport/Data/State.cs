﻿namespace SWD_Transport.Data
{
    public class State //: IComparable<State>
    {
        public Stop Stop { get; set; }
        public Time Time { get; set; }
        public TransportLine Line { get; set; }
        //public double Eval { get; set; }

        public State(Stop st, Time dt, TransportLine tl)
        {
            Stop = st;
            Time = dt;
            Line = tl;
            //Eval = double.MaxValue;
        }

        //public int CompareTo(State other)
        //{
        //    //TODO - potrzebny comparator
        //    if (ReferenceEquals(this, other)) return 0;
        //    if (ReferenceEquals(null, other)) return 1;
        //    return this.Equals(other)?0:1;
        //    //return Eval.CompareTo(other.Eval);
        //    //return Stop.CompareTo(other.Eval);
        //}

        public override string ToString()
        {
            return Stop.Name.ToString() + ", " + Time + ", " + Line.Name + " )";
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((State)obj);
        }

        protected bool Equals(State other)
        {
            return Equals(Stop, other.Stop) && Equals(Time, other.Time) && Equals(Line, other.Line);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (Stop != null ? Stop.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Time != null ? Time.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Line != null ? Line.GetHashCode() : 0);
                return hashCode;
            }
        }
    }
}
