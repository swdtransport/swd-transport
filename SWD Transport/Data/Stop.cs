﻿namespace SWD_Transport.Data
{
    public class Stop
    {
        public string Name { get; set; }

        public Stop()
        {
        }

        public Stop(string name)
        {
            Name = name;
        }

        protected bool Equals(Stop other)
        {
            return string.Equals(Name, other.Name);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((Stop) obj);
        }

        public override int GetHashCode()
        {
            return (Name != null ? Name.GetHashCode() : 0);
        }

        public override string ToString()
        {
            return Name;
        }
    }
}