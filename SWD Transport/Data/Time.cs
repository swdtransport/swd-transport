﻿using System;

namespace SWD_Transport.Data
{
    public class Time : IComparable<Time>
    {
        public int Hour { get; set; }
        public int Minute { get; set; }

        public int MinutesFromMidnight => 60 * Hour + Minute;

        public Time()
        {
        }

        public Time(int hour, int minute)
        {
            Hour = hour;
            Minute = minute;
        }

        public Time(DateTime dateTime) : this(dateTime.Hour, dateTime.Minute)
        {
        }

        public DateTime ToDateTime(int year, int month, int day)
        {
            return new DateTime(year, month, day, Hour, Minute, 0);
        }

        public void FixFormat()
        {
            int hours = Minute / 60;
            Hour = Hour + hours;
            Minute = Minute - hours * 60;

            if (Minute < 0)
            {
                Minute = Minute + 60;
                Hour = Hour - 1;
            }
        }

        public int CompareTo(Time other)
        {
            if (ReferenceEquals(this, other)) return 0;
            if (ReferenceEquals(null, other)) return 1;
            return MinutesFromMidnight - other.MinutesFromMidnight;
        }

        public static bool operator <(Time s1, Time s2)
        {
            return s1.MinutesFromMidnight < s2.MinutesFromMidnight;
        }

        public static bool operator <=(Time s1, Time s2)
        {
            return s1.MinutesFromMidnight <= s2.MinutesFromMidnight;
        }

        public static bool operator >(Time s1, Time s2)
        {
            return s1.MinutesFromMidnight > s2.MinutesFromMidnight;
        }

        public static bool operator >=(Time s1, Time s2)
        {
            return s1.MinutesFromMidnight >= s2.MinutesFromMidnight;
        }

        public static bool operator ==(Time s1, Time s2)
        {
            if (ReferenceEquals(s1, null) && ReferenceEquals(s2, null))
                return false;
            if (ReferenceEquals(s1, null) || ReferenceEquals(s2, null))
                return false;
            return s1.Equals(s2);
        }

        public static bool operator !=(Time s1, Time s2)
        {
            return !(s1 == s2);
        }

        protected bool Equals(Time other)
        {
            return Hour == other.Hour && Minute == other.Minute;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((Time) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (Hour * 397) ^ Minute;
            }
        }

        public override string ToString()
        {
            return $"{Hour:D2}:{Minute:D2}";
        }
    }
}