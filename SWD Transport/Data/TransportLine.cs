﻿namespace SWD_Transport.Data
{
    public class TransportLine
    {
        public string Name { get; set; }

        public TransportLine()
        {
        }


        public TransportLine(string name)
        {
            Name = name;
        }

        protected bool Equals(TransportLine other)
        {
            return string.Equals(Name, other.Name);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((TransportLine) obj);
        }

        public override int GetHashCode()
        {
            return (Name != null ? Name.GetHashCode() : 0);
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
