﻿using System;
using System.Collections.Generic;
using System.Linq;
using SWD_Transport.Data;

namespace SWD_Transport.Logic
{
    public class Solver
    {
        private readonly MapData _mapData;
        private readonly MapWrapper _mapWrapper;
        private Time _currentBest;
        private readonly double _changeCoefficient;
        private readonly Dictionary<State, Tuple<Decision, double, Connection>> _optimalDecisionsForState;

        public Solver(MapData mapData, double changeCoefficient = 0)
        {
            //Console.WriteLine("Create Solver");
            _currentBest = new Time(0, 0);
            _changeCoefficient = changeCoefficient;
            _optimalDecisionsForState = new Dictionary<State, Tuple<Decision, double, Connection>>();
            _mapData = mapData;
            _mapWrapper = new MapWrapper(mapData);
        }

        //tworzy listę stanów na podstawie początku połączenia

        private static List<State> CreateStatesList(List<Connection> connections)
        {

            return connections.Select(connection => new State(connection.End, connection.EndTime, connection.Line)).ToList();
        }


        public List<Connection> CalculateRoute(Stop startingStop, DateTime startTime, Stop destinationStop,
            DateTime endTime)
        {
            try
            {
                Time _endTime = new Time(endTime);
                Time _oldStartTime = new Time(startTime);
                Time _startTime = new Time(startTime - new TimeSpan(1,0,0));
                //Console.WriteLine("CalculateRoute");
                DayOfWeek dayOfWeek = endTime.DayOfWeek;
                if (!endTime.DayOfWeek.Equals(startTime.DayOfWeek))
                    _startTime = new Time(0,0);
                //Console.WriteLine("Calculating");

                List<State> states = CreateStatesList(_mapWrapper.GetConnectionsFiltered(destinationStop,dayOfWeek,_endTime, _startTime));
                states = states.Distinct().ToList();
                //dodanie "wysiadania i czekania" na koniec
                foreach (State state in states)
                {
                    _optimalDecisionsForState.Add(state, new Tuple<Decision, double, Connection>(new Decision(state.Stop, new TransportLine("jesteś u celu")), _endTime.MinutesFromMidnight - state.Time.MinutesFromMidnight, new Connection(state.Stop, state.Stop, state.Time, _endTime, new TransportLine("jesteś u celu"), dayOfWeek)));
                }
                List<Stop> stopsFromStates = GetStopsFromStates(states);





                states =
                    CreateStatesListEnd(_mapWrapper.GetPreviousConnections(destinationStop, dayOfWeek,
                        _endTime,
                        _startTime));


                bool continueTesting = true;
                //int tempIt = 0;


                bool foundBest = true;
                while (continueTesting && foundBest)
                {
                    foundBest = false;
                    List<Connection> connections = new List<Connection>();
                    foreach (State state in states)
                    {
                        ////Console.WriteLine(tempIt);tempIt++;
                        //tu trzeba zrobić jedynie decyzje prowadzące do poprzednich stanów (czyli do states, chyba)
                        List<Connection> possibleDecisions = _mapWrapper.GetConnectionsBetween(state.Stop,
                            stopsFromStates,
                            dayOfWeek, _endTime,
                            state.Time); //zwraca możliwe decyzje do podjęcia z tego stanu
                       
                        //sprawdzamy, czy jesteśmy na przyst. początkowym i czy jest to lepszy stan, niż ostatni
                        if (state.Stop.Equals(startingStop) && _currentBest < state.Time)
                            _currentBest = state.Time;
                        if (!_optimalDecisionsForState.ContainsKey(state) && possibleDecisions.Count > 0) //TODO: tego pierwszego też nie powinno być :< :< :<//
                        //if (possibleDecisions.Count > 0) 
                        {
                            var tup = FindBest(state.Line, possibleDecisions, state.Time, _endTime, _startTime, startingStop);
                            _optimalDecisionsForState.Add(state, tup);
                            foundBest = true;
                        }

                        //dodaj połączenia z przystanku
                        connections.AddRange(_mapWrapper.GetPreviousConnections(state.Stop, dayOfWeek,
                            state.Time, _currentBest < _startTime ? _startTime : _currentBest));
                    }
                    connections = connections.Distinct().ToList();
                    stopsFromStates = GetStopsFromStates(states);
                    states = CreateStatesListEnd(connections); //tutaj kolejne stany dla każdego aktualnego stanu
                    continueTesting = CheckIfContinueTesting(states, startTime);
                }
                //Console.WriteLine("Finished");
                return CreateResultList(startingStop, destinationStop, _oldStartTime);
                //try
                //{
                //    return CreateResultList(startingStop, destinationStop);
                //}
                //catch (Exception e) //just in case 
                //{
                //    return new List<Connection>();
                //}
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return new List<Connection>();
                //throw;
            }
        }

        private List<Stop> GetStopsFromStates(List<State> states)
        {
            List<Stop> result = new List<Stop>();
            foreach (State state in states)
            {
                result.Add(state.Stop);
            }
            return result.Distinct().ToList();
        }


        //tworzy listę stanów na podstawie końca połączenia
        private List<State> CreateStatesListEnd(List<Connection> connections)
        {
            List<State> result = new List<State>();
            foreach (Connection connection in connections)
            {
                result.Add(new State(connection.End, connection.EndTime, connection.Line));
            }
            //return connections.Select(connection => new State(connection.End, connection.EndTime, connection.Line)).ToList();
            foreach (State state in _optimalDecisionsForState.Keys)
            {
                if (result.Contains(state))
                    result.Remove(state);
            }
            return result.Distinct().ToList();
        }


        //zwraca najlepszą decyzję: <dezycja, wartość decyzji, połączenie
        private Tuple<Decision, double, Connection> FindBest(TransportLine currentLine, List<Connection> connections,
            Time timeOnStop, Time endTime, Time startTime, Stop startStop)
        {
            //Console.WriteLine("FindBest");
            double currentBest = double.MaxValue;
            int indexOfBest = -1;

            var connectionsCount = connections.Count;
            for (var i = 0; i < connectionsCount; i++)
            {
                Connection connection = connections[i];
                //koszt przejazdu danym połączeniem
                //uwaga: koszt dojazdu w przypadku linii start (czyli chęci wybrania autobusu z początkowego przystanku) zawsze wynosi 0
                double temp =
                    (connection.EndTime.MinutesFromMidnight -
                     timeOnStop.MinutesFromMidnight) + //TODO: dodać operator - do Time
                    _changeCoefficient * ((currentLine.Equals(connection.Line) || connection.Line.Equals(Utils.Walking) || connection.Start.Equals(startStop)) ? 0 : 1); //kara za przesiadkę
                //dodanie kosztów w przód
                State tempState = new State(connection.End, connection.EndTime, connection.Line);
                if (_optimalDecisionsForState.ContainsKey(tempState)) //TODO: to mi się nie podoba, tego nie może być matematycznie, rozważamy decyzję wyłącznie do znanych już stanów...ale się pojawia, także muszę nad tym potem posiedzieć (ale działa :D )
                    temp += _optimalDecisionsForState[tempState].Item2;
                else
                {
                    temp += endTime.MinutesFromMidnight - startTime.MinutesFromMidnight;
                }

                if (temp < currentBest)
                {
                    currentBest = temp;
                    indexOfBest = i;
                }
            }
            //Console.WriteLine(connectionsCount);
            //Console.WriteLine(indexOfBest);
            return new Tuple<Decision, double, Connection>(
                new Decision(connections[indexOfBest].End, connections[indexOfBest].Line), currentBest,
                connections[indexOfBest]);
        }

        //sprawdza, czy musimy kontynuować obliczenia
        private bool CheckIfContinueTesting(List<State> lista, DateTime stopTime)
        {
//            Console.WriteLine("CheckIfContinueTesting");
            //jak już nie ma nowych stanów
            if (lista.Count == 0)
                return false;
            //jak wszystkie nowe stany są zbyt wczesne
            Time currStopTime = new Time(stopTime) > _currentBest ? new Time(stopTime) : _currentBest;
            return lista.Any(state => state.Time > currStopTime);
        }


        //zwraca optymalne decyzje
        private List<Connection> CreateResultList(Stop startStop, Stop endStop, Time startTime)
        {
            //Console.WriteLine("CreateResultList");
            List<Connection> result = new List<Connection>();
            Tuple<Decision, double, Connection> nextConnection = FindOptimalStart(startStop);
            if (nextConnection == null || nextConnection.Item3.StartTime < startTime)
                return result;
            result.Add(nextConnection.Item3);
            State nextState = new State(nextConnection.Item1.Stop, nextConnection.Item3.EndTime,
                nextConnection.Item1.Line);
            while (!nextState.Stop.Equals(endStop))
            {
                nextConnection = _optimalDecisionsForState[nextState];
                result.Add(nextConnection.Item3);
                nextState = new State(nextConnection.Item1.Stop, nextConnection.Item3.EndTime,
                    nextConnection.Item1.Line);
            }
            return result;
        }


        //znajduje optymalny start (decyzję i połączenie)
        private Tuple<Decision, double, Connection> FindOptimalStart(Stop startStop)
        {
            //Console.WriteLine("FindOptimalStart");
            Tuple<Decision, double, Connection> result = null;
            double currMin = Double.MaxValue;
            foreach (KeyValuePair<State, Tuple<Decision, double, Connection>> keyValuePair in _optimalDecisionsForState)
            {
                if (keyValuePair.Key.Stop.Equals(startStop))
                    if (keyValuePair.Value.Item2 < currMin)
                    {
                        currMin = keyValuePair.Value.Item2;
                        result = keyValuePair.Value;
                    }
            }
            return result;
        }
    }
}