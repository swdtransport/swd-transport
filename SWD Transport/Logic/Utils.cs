﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using SWD_Transport.Data;

namespace SWD_Transport.Logic
{
    public static class Utils
    {
        public static readonly TransportLine Walking = new TransportLine("pieszo");

        public static bool SaveData(MapData mapData, Stream stream)
        {
            try
            {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(MapData));
                StreamWriter streamWriter = new StreamWriter(stream);
                xmlSerializer.Serialize(streamWriter, mapData);
                streamWriter.Close();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool SaveData(MapData mapData, string filename)
        {
            try
            {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(MapData));
                StreamWriter streamWriter = new StreamWriter(Directory.GetCurrentDirectory() + @"\" + filename);
                mapData.RemoveWalks();
                xmlSerializer.Serialize(streamWriter, mapData);
                mapData.AddWalks();
                streamWriter.Close();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static MapData ReadData(Stream stream)
        {
            try
            {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(MapData));
                MapData d = xmlSerializer.Deserialize(stream) as MapData;
                stream.Close();
                d?.ResolveDepenencies();
                d?.AddWalks();
                return d;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static MapData ReadData(string filename)
        {
            StreamReader streamReader = new StreamReader(Directory.GetCurrentDirectory() + @"\" + filename);
            return ReadData(streamReader.BaseStream);
        }

        public static List<Connection> CalculateRoute(MapData mapData, Stop startStop, DateTime starTime, Stop endStop,
            DateTime endTime, double changecoefficient)
        {
            try
            {
                return new Solver(mapData, changecoefficient).CalculateRoute(startStop, starTime, endStop, endTime);
            }
            catch (Exception e)
            {
                return new List<Connection>();
            }
        }

        public static List<ConnectionsGroup> GroupConnections(List<Connection> connections)
        {
            List<List<Connection>> grouped = new List<List<Connection>>();
            if (connections.Count > 0)
            {
                TransportLine lastLine = null;
                foreach (Connection connection in connections)
                {
                    if (!connection.Line.Equals(lastLine))
                        grouped.Add(new List<Connection>());
                    grouped[grouped.Count - 1].Add(connection);
                    lastLine = connection.Line;
                }
            }
            return grouped.Select(group => new ConnectionsGroup(@group)).ToList();
        }

        public static MapData TestData()
        {
            MapData mapData = new MapData();
            TransportLine linia131 = new TransportLine("131");
            TransportLine linia141 = new TransportLine("141");
            TransportLine linia33 = new TransportLine("33");
            TransportLine liniaD = new TransportLine("D");
            Stop kochanowskiego = new Stop("Kochanowskiego");
            Stop bujwida = new Stop("Bujwida");
            Stop grunwald = new Stop("Plac Grunwaldzki");
            Stop mostGrunwaldzki = new Stop("Most Grunwaldzki");

            Connection connection1 = new Connection(kochanowskiego, bujwida, new Time(8, 1), new Time(8, 5), linia131,
                DayOfWeek.Tuesday);
            Connection connection2 = new Connection(kochanowskiego, bujwida, new Time(8, 10), new Time(8, 15), linia141,
                DayOfWeek.Tuesday);
            Connection connection3 = new Connection(kochanowskiego, bujwida, new Time(8, 2), new Time(8, 5), linia33,
                DayOfWeek.Tuesday);
            Connection connection4 = new Connection(kochanowskiego, grunwald, new Time(8, 7), new Time(8, 17), liniaD,
                DayOfWeek.Tuesday);
            Connection connection5 = new Connection(bujwida, grunwald, new Time(8, 15), new Time(8, 27), linia131,
                DayOfWeek.Tuesday);
            Connection connection6 = new Connection(bujwida, grunwald, new Time(8, 20), new Time(8, 30), linia141,
                DayOfWeek.Tuesday);
            Connection connection61 = new Connection(bujwida, grunwald, new Time(8, 20), new Time(8, 28), linia131,
                DayOfWeek.Tuesday);
            Connection connection7 = new Connection(bujwida, grunwald, new Time(8, 15), new Time(8, 30), linia33,
                DayOfWeek.Tuesday);
            Connection connection8 = new Connection(grunwald, mostGrunwaldzki, new Time(8, 30), new Time(8, 35),
                linia33,
                DayOfWeek.Tuesday);

            Connection walk1 = new Connection(kochanowskiego, bujwida, new Time(0, 0), new Time(0, 15), Walking,
                DayOfWeek.Monday);
            Connection walk2 = new Connection(bujwida, grunwald, new Time(0, 0), new Time(0, 15), Walking,
                DayOfWeek.Monday);
            Connection walk3 = new Connection(grunwald, mostGrunwaldzki, new Time(0, 0), new Time(0, 15), Walking,
                DayOfWeek.Monday);
            Connection walk4 = new Connection(mostGrunwaldzki, grunwald, new Time(0, 0), new Time(0, 15), Walking,
                DayOfWeek.Monday);
            Connection walk5 = new Connection(grunwald, bujwida, new Time(0, 0), new Time(0, 15), Walking,
                DayOfWeek.Monday);
            Connection walk6 = new Connection(bujwida, kochanowskiego, new Time(0, 0), new Time(0, 15), Walking,
                DayOfWeek.Monday);

            mapData.Lines.Add(linia131);
            mapData.Lines.Add(linia141);
            mapData.Lines.Add(linia33);
            mapData.Lines.Add(liniaD);

            mapData.Stops.Add(kochanowskiego);
            mapData.Stops.Add(bujwida);
            mapData.Stops.Add(grunwald);
            mapData.Stops.Add(mostGrunwaldzki);

            mapData.Connections.Add(connection1);
            mapData.Connections.Add(connection2);
            mapData.Connections.Add(connection3);
            mapData.Connections.Add(connection4);
            mapData.Connections.Add(connection5);
            mapData.Connections.Add(connection6);
            mapData.Connections.Add(connection61);
            mapData.Connections.Add(connection7);
            mapData.Connections.Add(connection8);

            mapData.Connections.Add(walk1);
            mapData.Connections.Add(walk2);
            mapData.Connections.Add(walk3);
            mapData.Connections.Add(walk4);
            mapData.Connections.Add(walk5);
            mapData.Connections.Add(walk6);

            mapData.AddWalks();

            return mapData;
        }
    }
}