﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using SWD_Transport.Data;
using SWD_Transport.Logic;

namespace SWD_Transport.View
{
    public class CalculateCommand : ICommand
    {
        private readonly Func<MapData> _mapDataProvider;
        private readonly Func<Tuple<Stop, DateTime>> _startDataProvider;
        private readonly Func<Tuple<Stop, DateTime>> _endDataProvider;
        private readonly Func<double> _changeCoefficientProvider;
        private readonly Action _before;
        private readonly Action<List<Connection>> _afterCalculateAction;

        public CalculateCommand(Func<MapData> mapDataProvider, Func<Tuple<Stop, DateTime>> startDataProvider,
            Func<Tuple<Stop, DateTime>> endDataProvider, Func<double> changeCoefficientProvider, Action before,
            Action<List<Connection>> afterCalculateAction)
        {
            _mapDataProvider = mapDataProvider;
            _startDataProvider = startDataProvider;
            _endDataProvider = endDataProvider;
            _changeCoefficientProvider = changeCoefficientProvider;
            _before = before;
            _afterCalculateAction = afterCalculateAction;
        }


        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            Task.Factory.StartNew(() =>
            {
                _before.Invoke();
                Thread.Sleep(500);
                _afterCalculateAction.Invoke(CalculationOutput());
            });
        }

        private List<Connection> CalculationOutput()
        {
            var mapData = _mapDataProvider.Invoke();
            var startData = _startDataProvider.Invoke();
            var endData = _endDataProvider.Invoke();
            var changeCoefficient = _changeCoefficientProvider.Invoke();


            //Console.WriteLine("changeCoefficient: " +changeCoefficient);
            bool b = true;
            b &= mapData != null;
            b &= startData.Item1 != null;
            b &= endData.Item1 != null;
            b &= DateTime.Compare(startData.Item2, endData.Item2) < 0;
            return b
                ? Utils.CalculateRoute(mapData, startData.Item1, startData.Item2, endData.Item1, endData.Item2, changeCoefficient)
                : new List<Connection>();
        }

        public event EventHandler CanExecuteChanged;

        protected virtual void OnCanExecuteChanged()
        {
            CanExecuteChanged?.Invoke(this, EventArgs.Empty);
        }
    }
}