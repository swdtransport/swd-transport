﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using SWD_Transport.Data;
using SWD_Transport.Logic;
using SWD_Transport.Properties;

namespace SWD_Transport.View
{
    public class MainWindowViewModel : INotifyPropertyChanged
    {
        private readonly Dispatcher _dispatcher;
        public MapData MapData { get; set; }
        public List<Stop> Stops => MapData?.Stops.OrderBy(s => s.Name).ToList();

        public DateTime StartTime { get; set; } = DateTime.Now;
        public DateTime EndTime { get; set; } = DateTime.Now;
        public double ChangeCoefficient { get; set; } = 0;

        public Stop StartStop { get; set; }
        public Stop EndStop { get; set; }

        public ObservableCollection<ConnectionsGroup> OutputConnections { get; set; }

        public Visibility ConnectionsPresent => OutputConnections.Count > 0 ? Visibility.Collapsed : Visibility.Visible;

        public ICommand Calculate { get; set; }
        public ICommand ReadMap { get; set; }

        public bool Busy { get; set; }

        public MainWindowViewModel(Dispatcher dispatcher)
        {
            _dispatcher = dispatcher;
            MapData = Utils.TestData();
            Utils.SaveData(MapData, "example.xml");
            OutputConnections = new ObservableCollection<ConnectionsGroup>();
            Calculate = new CalculateCommand(() => MapData,
                () => Tuple.Create(StartStop, StartTime),
                () => Tuple.Create(EndStop, EndTime),
                () => ChangeCoefficient,
                BeforeCalculation,
                AfterCalculation);
            ReadMap = new OpenFileCommand(AfterOpenFile);
        }

        private void AfterOpenFile(MapData mapData)
        {
            MapData = mapData;
            OnPropertyChanged(nameof(Stops));
        }

        private void BeforeCalculation()
        {
            _dispatcher.BeginInvoke(DispatcherPriority.Render, new Action(() =>
            {
                Busy = true;
                OnPropertyChanged(nameof(Busy));
            }));
        }

        private void AfterCalculation(List<Connection> connections)
        {
            _dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            {
                OutputConnections.Clear();
                connections.ForEach(c => c.DayOfWeek = StartTime.DayOfWeek);
                Utils.GroupConnections(connections).ForEach(OutputConnections.Add);
                Busy = false;
                OnPropertyChanged(nameof(Busy));
                OnPropertyChanged(nameof(ConnectionsPresent));
            }));
        }

        #region PropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}