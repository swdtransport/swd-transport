﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Microsoft.Win32;
using SWD_Transport.Data;
using SWD_Transport.Logic;

namespace SWD_Transport.View
{
    class OpenFileCommand : ICommand
    {
        private readonly Action<MapData> _action;

        public OpenFileCommand(Action<MapData> action)
        {
            _action = action;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            MapData mapData = null;
            OpenFileDialog openFileDialog = new OpenFileDialog
            {
                Filter = "Plik XML (*.xml)|*.xml",
                CheckPathExists = true
            };
            if (openFileDialog.ShowDialog() == true)
            {
                mapData = Utils.ReadData(openFileDialog.OpenFile());
                if (mapData == null)
                {
                    MessageBox.Show("Połączenia nie zostały wczytane", "Błąd odczytu pliku");
                }
                else
                {
                    string s = mapData.Lines.Aggregate("", (current, linie) => current + linie.Name + ", ");
                    if (mapData.Lines.Count > 0)
                        s = s.Substring(0, s.Length - 2);
                    MessageBox.Show("Wczytano:\n" + s, "Zakończono wczytywanie linii");
                }
            }
            _action.Invoke(mapData);
        }

        public event EventHandler CanExecuteChanged;
    }
}